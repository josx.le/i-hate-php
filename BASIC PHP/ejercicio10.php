<?php 
//Numeros aleatorios

$numeroaleatorio = rand(1,100);
echo $numeroaleatorio."<br/>";

//Mayusculas

$nombre ="José López";
$mayuscula = strtoupper($nombre);
echo $mayuscula;

//Valor absoluto 

echo abs(-4.2);
echo abs(5);
echo abs(-5);

//sumar

function suma($a, $b) {
    return $a + $b;
}

$resultado = suma(5, 3);
echo "La suma es: $resultado";

//calcular el área de un rectángulo

class Rectangulo {
    public $base;
    public $altura;

    public function calcularArea() {
        return $this->base * $this->altura;
    }
}

$rectangulo = new Rectangulo();
$rectangulo->base = 8;
$rectangulo->altura = 4;
echo "El área del rectángulo es: " . $rectangulo->calcularArea();

//verificar si un número es par o impar

function esPar($numero) {
    return ($numero % 2 == 0) ? true : false;
}

$numero = 7;
if (esPar($numero)) {
    echo "$numero es par";
} else {
    echo "$numero es impar";
}

//invertir una cadena de texto

class CadenaInvertida {
    public function invertir($cadena) {
        return strrev($cadena);
    }
}

$cadenaInvertida = new CadenaInvertida();
$texto = "Hola, mundo!";
echo "Texto original: $texto <br>";
echo "Texto invertido: " . $cadenaInvertida->invertir($texto);

?>