<?php 

//Arreglos
$flores = array("margarita", "rosa", "Clavel");
print_R($flores);
//Se puede poner que indice desea
$frutas = array("f"=>"fresa","p"=>"pera","u"=>"uva");
print_r($frutas);
//imprimir solo un elemtno
echo $flores[1];
echo $frutas["u"];
//imprimir todos los elementos con for
for($i = 0; $i < 3; $i++) {
    echo $flores[$i];
}
//imprimir todos los elementos con foreach
foreach($frutas as $indice=> & $valor) {
    //mostrar indice
    echo $indice."<br/>";
    //mostrar los valores del arreglo
    echo $valor."<br/>";
    //mostrar todo
    echo $frutas[$indice]."<br/>";
}

?>